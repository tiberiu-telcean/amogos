.RECIPEPREFIX = >
.PHONY = all clean doc main

all: main doc

clean:
>	rm *.o *.elf *.bin

doc: doxy.conf .
>	doxygen doxy.conf

main: kernel/ bootloader/ tools/
>	tup
