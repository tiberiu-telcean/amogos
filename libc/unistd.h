#pragma once
#define _UNISTD_H
#define _UNISTD_H_

#define _POSIX_VERSION 200809L
#define _POSIX2_VERSION 200809L
#define _POSIX_TIMERS 200809L

#include <_decl_NULL.h>
#include <stdint.h>
#include <sys/types.h>
#include <syscall.h>

extern pid_t getpid(void);
extern pid_t fork(void);
extern int execv(const char *, char *const argv[]);
extern int execve(const char *, char *const argv[], char *const envp[]);
extern int execvp(const char *, char *const argv[]);