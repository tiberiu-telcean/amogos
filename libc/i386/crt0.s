.section .text

.global _start
_start:
    mov %ebp, 0
    pushd %ebp # eip
    pushd %ebp # ebp
    mov %ebp, %esp

    pushd %esi
    pushd %edi

    call _init

    popd %edi
    popd %esi

    call main
    mov %edi, %eax
    pushd %edi
    call exit
.size _start, .-_start
