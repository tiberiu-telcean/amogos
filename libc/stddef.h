#pragma once
#define _STDDEF_H
#define _STDDEF_H_

#include <stdint.h>

#define _PTRDIFF_T
typedef long int ptrdiff_t;
#include <_decl_size.h>

#define offsetof(type, member) (size_t) & (((type *)0)->member)

#include <_decl_NULL.h>

