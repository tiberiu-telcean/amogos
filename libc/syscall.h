/**
 * @file syscall.h
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * define syscall numbers and an assembly function for setting the parameters.
 */

#pragma once
#include <stdint.h>

#define SYS_READ 0
#define SYS_WRITE 1
#define SYS_SLEEP 2
#define SYS_NOTE 3
#define SYS_EXIT 4
#define SYS_SPAWN 5
#define SYS_SIGNAL 6
#define SYS_GETPID 7
#define SYSNUM 8

#define NUSED 0

extern uint32_t csc(uint32_t eax, uint32_t ebx, uint32_t ecx, uint32_t edx);
