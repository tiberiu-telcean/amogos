#pragma once
#define _STDBOOL_H
#define _STDBOOL_H_

#define __bool_true_false_are_defined 1
#include <stdint.h>

typedef uint8_t bool;

#define true (bool)1
#define false (bool)0
