#include <ctype.h>

int isspace(int c) {
  switch (c) {
  case (' '):
  case ('\t'):
  case ('\n'):
  case ('\v'):
  case ('\f'):
  case ('\r'):
    return 1;
  default:
    return 0;
  }
}

int isdigit(int c) {
  if ('0' <= c && c <= '9') {
    return 1;
  }
  return 0;
}
