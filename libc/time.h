#pragma once
#define _TIME_H
#define _TIME_H_

#include <stdint.h>

#include <_decl_NULL.h>
#define CLOCKS_PER_SEC 1000
typedef uint8_t clock_t;
typedef int32_t time_t;
struct tm {
  int tm_sec;
  int tm_min;
  int tm_hour;
  int tm_mday;
  int tm_mon;
  int tm_year;
  int tm_wday;
  int tm_yday;
  int tm_isdst;
};
#include <_decl_size.h>
