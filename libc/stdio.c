/**
 * @file stdio.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Writing and reading from streams
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <errno.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syscall.h>

FILE *stdin = &(FILE){.fd = 0, .error = 0};
FILE *stdout = &(FILE){.fd = 1, .error = 0};
FILE *stderr = &(FILE){.fd = 2, .error = 0};

size_t fwrite(const void *__restrict buf, size_t size, size_t count,
              FILE *__restrict stream) {
  size_t wrote;
  wrote =
      csc(SYS_WRITE, (uint32_t)buf, size * count, (uint32_t)stream->fd) / size;
  return wrote;
}

size_t fread(void *buf, size_t size, size_t count, FILE *stream) {
  size_t read;
  read =
      csc(SYS_READ, (uint32_t)buf, size * count, (uint32_t)stream->fd) / size;
  return read;
}

int fclose(FILE *stream) { return 0; }
FILE *fopen(const char *path, const char *mode) {
  FILE *f = (FILE *)malloc(sizeof(FILE));
  f->eof = false;
  for (int8_t i = 0; mode[i] != 0; i++) {
    switch (mode[i]) {
    case ('r'):
      f->rw = false;
      break;
    case ('w'):
      f->rw = true;
      break;
    case ('+'):
      f->rw = true;
      break;
    case ('a'):
      f->rw = true;
      f->eof = true;
      break;
    case ('b'):
      f->binary = true;
      break;
    }
  }
  // TODO: openfd syscall
  // f->fd = csc(SYS_OPENFD, (uint32_t)path, f->rw, NUSED);
  // if(f->fd<0) { errno=f->fd; }
  return f;
}

int fseek(FILE *stream, long int off, int origin) {
  long int start = 0;
  switch (origin) {
  case (SEEK_SET):
    start = stream->pos;
    break;
  case (SEEK_CUR):
    start = 0;
    break;
  }
  stream->pos = start + off;
  // TODO: seek syscall
  // return csc(SYS_SEEK, stream->pos, NUSED, NUSED);
  return 0;
}

long int ftell(FILE *stream) { return stream->pos; }

uint32_t puts(char *buf) {
  uint32_t ret;
  ret = fwrite(buf, 1, strlen(buf), stdout);
  ret = fwrite("\n", 1, 1, stdout);
  return ret;
}

int putc(int c, FILE *stream) { return fputc(c, stream); }

int fputc(int c, FILE *stream) {
  char buf[1];
  buf[0] = (unsigned char)c;
  stream->error = fwrite(buf, 1, 1, stream);
  return stream->error ? c : EOF;
}

int ferror(FILE *stream) { return stream->error; }

int putchar(int c) { return fputc(c, stdout); }

int fprintf(FILE *stream, const char *fmt, ...) {
  va_list ap;
  int ret;
  va_start(ap, fmt);
  ret = vfprintf(stream, fmt, ap);
  va_end(ap);
  return ret;
}

int vfprintf(FILE *stream, const char *fmt, va_list ap) {
  char buf[512];
  memset(buf, 0, 512);
  return fwrite(buf, 1, vsprintf(buf, fmt, ap), stream);
}

int vprintf(const char *fmt, va_list ap) { return vfprintf(stdout, fmt, ap); }

int vsprintf(char *s, const char *fmt, va_list ap) {
  char temp_s[512];
  int temp_i, iter;
  bool in_format = false;
  uint32_t phys_index = 0;
  for (int i = 0; fmt[i] != 0; i++) {
    memset(temp_s, 0, 512);
    if (in_format) {
      switch (fmt[i]) {
      case 'd':
        temp_i = va_arg(ap, int);
        iter = 0;
        while (temp_i) {
          temp_s[10 - iter] = temp_i % 10 + '0';
          iter++;
          temp_i /= 10;
        }
        strcpy(s + phys_index, temp_s + (11 - iter));
        phys_index += iter;
        in_format = false;
        break;
      }
      continue;
    }
    switch (fmt[i]) {
    case '%':
      in_format = true;
      break;
    default:
      s[phys_index] = fmt[i];
      phys_index++;
      break;
    }
  }
  return phys_index+1;
}

int sprintf(char *s, const char *fmt, ...) {
  va_list ap;
  int ret;
  va_start(ap, fmt);
  ret = vsprintf(s, fmt, ap);
  va_end(ap);
  return ret;
}

int printf(const char *fmt, ...) {
  va_list ap;
  int ret;
  va_start(ap, fmt);
  ret = vprintf(fmt, ap);
  va_end(ap);
  return ret;
}

char *msg[N_ERRORS];

void perror(const char *msg) {}

int fflush(FILE *stream) {
  return 0;
}
