#include <sys/types.h>
#include <syscall.h>
#include <unistd.h>

pid_t getpid(void) { return csc(SYS_GETPID, NUSED, NUSED, NUSED); }

pid_t fork(void) { return -1; }
int execv(const char *file, char *const argv[]) { return -1; }
int execve(const char *file, char *const argv[], char *const envp[]) {
  return -1;
}
int execvp(const char *file, char *const argv[]) { return -1; }