#pragma once
#define _STDIO_H
#define _STDIO_H_

#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef int16_t fd_t;

typedef struct FILE {
  long int pos;
  fd_t fd;
  bool error : 1;
  bool rw : 1;
  bool eof : 1;
  bool binary : 1;
} FILE;

#define EOF -1
#define SEEK_SET -31
#define SEEK_CUR -63
#define SEEK_END -127

#define FOPEN_MAX 32768

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

extern uint32_t puts(char *buf);
extern size_t fwrite(const void *buf, size_t size, size_t count, FILE *stream);
extern size_t fread(void *__restrict buf, size_t size, size_t count,
                    FILE *__restrict stream);
extern int fclose(FILE *stream);
extern FILE *fopen(const char *path, const char *mode);
extern int fseek(FILE *stream, long int off, int origin);
extern long int ftell(FILE *stream);
extern int vfprintf(FILE *stream, const char *fmt, va_list ap);
extern int vsprintf(char *s, const char *fmt, va_list ap);
extern int vprintf(const char *fmt, va_list ap);
extern int fprintf(FILE *stream, const char *fmt, ...);
extern int sprintf(char *s, const char *fmt, ...);
extern int printf(const char *fmt, ...);
extern int ferror(FILE *stream);
extern int fflush(FILE *stream);
extern int putc(int c, FILE *stream);
extern int fputc(int c, FILE *stream);
extern int putchar(int c);
extern void perror(const char *msg);
