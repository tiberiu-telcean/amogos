#pragma once
#define _SIGNAL_H
#define _SIGNAL_H_

#define SIGHUP 1
#define SIGSTOP 2
#define SIGCONT 3
#define SIGINT 4
#define SIGKILL 5
