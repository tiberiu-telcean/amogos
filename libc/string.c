/**
 * @author Tiberiu Telcean
 * @version 0.01
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

char *strcat(char *target, const char *source) {
  strcpy(target + strlen(target), source);
  return target;
}

char *strcpy(char *target, const char *source) {
  for (int i = 0; source[i] != 0; i++) {
    target[i] = source[i];
  }
  target[strlen(source)] = 0;
  return target;
}

unsigned long strlen(const char *string) {
  int i;
  for (i = 0; string[i] != 0; i++)
    ;
  return i;
}

char *itoa(int n) {
  char *result;
  int len = 0;
  int b = n;
  while (b) {
    len++;
    b /= 10;
  }
  if (len == 0) {
    len = 1;
  }
  result = (char *)malloc(sizeof(char) * (len + 1));
  result[len] = 0;
  if (n == 0) {
    result[0] = '0';
    return result;
  }
  while (n) {
    b++;
    result[len - b] = '0' + n % 10;
    n /= 10;
  }
  return result;
}

char *itoh(int n) {
  char *result = (char *)0x50000;
  int len = 0, temp = n;
  while (temp) {
    len++;
    temp /= 16;
  }
  if (len == 0) {
    len = 1;
  }
  memset(result, 0, 16);
  while (n) {
    temp++;
    if (n % 16 < 10)
      result[len - temp] = '0' + n % 16;
    else
      result[len - temp] = 'a' + (n % 16 - 10);
    n /= 16;
  }
  return result;
}

void *memcpy(void *target, const void *start, size_t size) {
  for (int i = 0; i < size; i++) {
    ((int8_t *)target)[i] = ((int8_t *)start)[i];
  }
  return target;
}

void *memset(void *target, int value, size_t size) {
  for (int i = 0; i < size; i++) {
    ((int8_t *)target)[i] = (int8_t)value;
  }
  return target;
}

char *strchr(const char *str, int ch) {
  int i;
  for (i = 0; str[i] != ch; i++) {
    if (str[i] == 0) {
      return NULL;
    }
  }
  return &((char *)str)[i];
}

int strcmp(const char *str1, const char *str2) {
  for (int i = 0; str1[i]; i++) {
    if (str1[i] != str2[i]) {
      return str1[i] < str2[i] ? -1 : 1;
    }
  }
  return 0;
}

size_t strcspn(const char *str1, const char *str2) {
  size_t i;
  for (i = 0; str1[i] != 0; i++) {
    if (strchr(str2, str1[i])) {
      break;
    }
  }
  return i;
}

size_t strspn(const char *str1, const char *str2) {
  size_t i;
  for (i = 0; str1[i] != 0; i++) {
    if (!strchr(str2, str1[i])) {
      break;
    }
  }
  return i;
}

char *strtok(char *str, const char *del) {
  static int index;
  static char *string;
  size_t len = 0;
  if (str) {
    index = 0;
    string = str;
  }
  size_t start = index;
  len = strspn(&string[start], del);
  start += len;
  len = strcspn(&string[start], del);
  index += len;
  index++;
  if (string[start] == 0) {
    return NULL;
  }
  string[index] = 0;
  index++;
  return &string[start];
}