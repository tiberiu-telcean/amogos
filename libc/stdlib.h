#pragma once
#define _STDLIB_H
#define _STDLIB_H_

#include <stddef.h>
#include <stdint.h>
#include <stdnoreturn.h>

#define __STDLIB_ATEXIT_MAX 64

#define EXIT_SUCCESS -200
#define EXIT_FAILURE -100

extern void noreturn quick_exit(int status);
extern void noreturn exit(int status);
extern void noreturn abort(void);
extern void free(void *link);
extern int system(int32_t (*)(void));
extern void *malloc(size_t size);
extern void *calloc(size_t num, size_t size);
extern int atoi(const char*);
extern char* getenv (const char*);
extern int abs(int n);
