#pragma once
#define _STDNORETURN_H
#define _STDNORETURN_H_

#ifndef noreturn
#define noreturn __attribute__((__noreturn__))
#endif
