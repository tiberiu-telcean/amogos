/**
 * @author Tiberiu Telcean
 * @version 0.01
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

typedef struct seg_hdr {
  size_t size;
  struct seg_hdr *next;
  struct seg_hdr *prev;
  bool free : 1;
} seg_hdr;

seg_hdr *heap = (seg_hdr *)0x400000;
seg_hdr *first_free = (seg_hdr *)0x400000;
seg_hdr *last_free = (seg_hdr *)0x400000;
size_t heap_size = 0;

void set_heap(ptrdiff_t offset);

void expand(size_t size) {
  heap_size++;
  set_heap(last_free - heap);
}

void free(void *link) {
  seg_hdr hdr;
  seg_hdr *c = link - sizeof(seg_hdr);
  memcpy(&hdr, c, sizeof(seg_hdr));
  hdr.free = true;
  memcpy(c, &hdr, sizeof(seg_hdr));

  if (c < first_free) {
    first_free = c;
  }
}

bool is_heap(void *link) {
  if ((void *)heap <= link && link <= (void *)heap + heap_size)
    return true;
  return false;
}

void *malloc(size_t _size) {
  seg_hdr *link;
  size_t size = _size + sizeof(seg_hdr);
  seg_hdr new_hdr;
  seg_hdr free_hdr;

  if(last_free + size + sizeof(seg_hdr) > heap+heap_size){
    expand((last_free + size + sizeof(seg_hdr))-(heap+heap_size));
  }

  for (link = first_free; !link->free && size > link->size;) {
    if ((void *)link >= (void *)heap + heap_size || link < heap) {
      return NULL;
    }
    if (link->next != NULL) {
      link = link->next;
    }
  }
  new_hdr.size = size;
  new_hdr.free = false;
  new_hdr.prev = link->prev;
  new_hdr.next = link + size;
  free_hdr.size = link->size - size;
  free_hdr.prev = link;
  free_hdr.next = link->next;
  free_hdr.free = true;
  if (link <= first_free)
    first_free = link + size;
  memcpy(link, &new_hdr, sizeof(seg_hdr));
  memcpy(link + size, &free_hdr, sizeof(seg_hdr));
  return link + sizeof(seg_hdr);
}

void *calloc(size_t num, size_t size){
  if(size==0 || num==0)
    return NULL;
  void *ret=malloc(size*num);
  memset(ret, 0, size*num);
  return ret;
}

void set_heap(ptrdiff_t offset) {
  size_t free_space = heap_size - offset;
  seg_hdr *wilderness = heap + offset;
  memset(heap + offset, 0, free_space);
  wilderness->free = true;
  if (last_free != wilderness) { // splitting wilderness
    wilderness->prev = last_free;
    last_free->next = heap + offset;
    last_free->size -= free_space;
  } else {
    wilderness->prev = NULL;
  }
  wilderness->next = NULL;
  wilderness->size = free_space;
  if (wilderness > last_free)
    last_free = wilderness;
}
