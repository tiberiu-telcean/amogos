.section .text
.global csc
csc:
    pushd %ebp
    mov %ebp, %esp
    pusha
    mov %eax, [%ebp+0x8]
    mov %ebx, [%ebp+0xc]
    mov %ecx, [%ebp+0x10]
    mov %edx, [%ebp+0x14]
    int 0x88
    mov [%ebp-4], %eax
    popa
    pop %ebp
    ret
