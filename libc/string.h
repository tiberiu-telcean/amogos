#pragma once
#define _STRING_H
#define _STRING_H_

#include <_decl_NULL.h>
#include <_decl_size.h>
#include <stddef.h>
#include <stdint.h>

extern void *memcpy(void *target, const void *source, size_t size);
extern void *memset(void *target, int value, size_t size);
extern char *strcat(char *target, const char *source);
extern char *strcpy(char *target, const char *source);
extern unsigned long strlen(const char *string);
extern char *itoa(int n);
extern char *itoh(int n);
extern char *strtok(char *str, const char *del);
extern int strcmp(const char *str1, const char *str2);
extern char *strchr(const char *str, int ch);
extern size_t strcspn(const char *str1, const char *str2);
extern size_t strspn(const char *str1, const char *str2);