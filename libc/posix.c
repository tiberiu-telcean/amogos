#include <syscall.h>
#include <unistd.h>

unsigned sleep(unsigned ticks) { return csc(SYS_SLEEP, ticks, NUSED, NUSED); }
