#include <stdlib.h>
#include <stdnoreturn.h>
#include <syscall.h>

extern void _fini(void);

void noreturn quick_exit(int status) {
  _fini();
  csc(SYS_EXIT, status, NUSED, NUSED);
  for (;;)
    ;
  asm("hlt");
};

void noreturn abort(void) {
  csc(SYS_EXIT, EXIT_FAILURE, NUSED, NUSED);
  for (;;)
    ;
  asm("hlt");
}

int system(int32_t (*eip)(void)) {
  csc(SYS_SPAWN, (uint32_t)eip, NUSED, NUSED);
  return 0;
}

typedef void (*__STDLIB_ATEXIT_T)(void);
__STDLIB_ATEXIT_T __STDLIB_atexit[__STDLIB_ATEXIT_MAX];

int atexit(void (*func)(void)) {
  for (int i = __STDLIB_ATEXIT_MAX - 1; i >= 0; i--) {
    if (__STDLIB_atexit[i] == NULL) {
      __STDLIB_atexit[i] = func;
      goto success;
    }
  }
  return -1;
success:
  return 0;
}

void exit(int status) {
  for (int i = 0; i < __STDLIB_ATEXIT_MAX; i++) {
    if (__STDLIB_atexit[i]) {
      __STDLIB_atexit[i]();
    } else {
      break;
    }
  }
  _fini();
  csc(SYS_EXIT, status, NUSED, NUSED);
  for (;;)
    asm("hlt");
}

#include <ctype.h>

int atoi(const char *str) {
  int i = 0, n = 0;
  for (; !isspace(str[i]); i++)
    ;
  if (str[i] == '-') {
    n = -n;
    i++;
  } else if (str[i] == '+') {
    i++;
  }
  for (; isdigit(str[i]); i++) {
    n *= 10;
    n += i - '0';
  }
  return n;
}

char *getenv(const char *name) { return NULL; }

int abs(int n) {
  if (n < 0)
    return -n;
  return n;
}
