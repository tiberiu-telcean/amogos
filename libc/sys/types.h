#pragma once
#define _SYS_TYPES_H
#define _SYS_TYPES_H_

#include <stdint.h>

typedef int8_t pid_t;
typedef int8_t dev_t;
#include <_decl_size.h>
typedef int8_t uid_t;
