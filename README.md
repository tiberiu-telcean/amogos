## Demo
![Qemu VM](/demo.png)
## Installation
Please do this in order.
### preparation
First set up some variables
```
export TARGET=i386-elf-amogos
export PREFIX="$HOME/opt/bin/cross/"
export PATH="$PREFIX/bin:$PATH"
export REPO="$HOME/amogos"
```
### binutils
```
cd binutils-gdb
patch <$REPO/binutils.patch
cd ..
mkdir build-binutils && cd build-binutils
../binutils-gdb/configure --target=$TARGET --prefix="$PREFIX" --with-sysroot="$REPO/userspace" --disable-nls --disable-werror
make
make install
```
### GCC
```
cd gcc-x.y.z
patch <$REPO/gcc.patch
cd ..
mkdir build-gcc && cd build-gcc
../gcc-x.y.z/configure --target=$TARGET --prefix="$PREFIX" --disable-nls --enable-languages=c --with-sysroot="$REPO/userspace"
make all-gcc all-target-libgcc
make install-gcc install-target-libgcc
```
### tup
```
git clone git://github.com/gittup/tup.git
cd tup
./build.sh install
```
### amogos
```
./create_root.sh
tup init
cd libc
make
cd ..
./qemu.sh
```
