/**
 * @file combine.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Combine two binary files while aligning to 256-byte disk sectors.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

char buf[512];
int main(int argc, char **argv) {
  FILE *output, *input;
  int read, sectors;
  if (argc < 3) {
    printf("Usage : %s output.bin file1.bin file2.bin ...", argv[0]);
    printf("Combine files aligning to disk sectors");
  }

  output = fopen(argv[1], "r");
  if (output != NULL)
    fclose(output);
  output = fopen(argv[1], "wb");

  for (int i = 2; i < argc; i++) {
    input = fopen(argv[i], "rb");
    sectors=0;
    if (input == NULL)
      exit(0);
    for (;;) {
      read = fread(buf, 1, 512, input);
      sectors++;
      if (read % 512) {
        fwrite(buf, 1, read, output);
        memset(buf, 0, 512);
        fwrite(buf, 1, 512 - read, output);
        break;
      } else if (read == 0) {
        break;
      } else {
        fwrite(buf, 1, 512, output);
      }
    }
    printf("%s: %d sectors\n", argv[i], sectors);
    fclose(input);
  }
  fclose(output);
}
