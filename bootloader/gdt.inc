gdt_pos  equ 0x10000
gdtd_pos equ 0x0fff0

gdt:
gdt_null:
    dd 0
    dd 0
gdt_code:
    dw 0xFFFF
    dw 0
    ; --
    db 0
    db 10011010b
    db 11001111b
    db 0
gdt_data:
    dw 0xFFFF
    dw 0
    ; --
    db 0
    db 10010010b
    db 11001111b
    db 0
gdt_ring3_code:
    dw 0xFFFF
    dw 0
    ; --
    db 0
    db 11111010b
    db 11011111b
    db 0
gdt_ring3_data:
    dw 0xFFFF
    dw 0
    ; --
    db 0
    db 11110010b
    db 11011111b
    db 0
gdt_tss:
    dd 0
    dd 0
gdt_end:
gdt_desc:
    dw gdt_end - gdt - 1
    dd gdt_pos
