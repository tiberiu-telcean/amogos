mov [drive], dl
cli
cld
xor ax, ax
mov es, ax
mov ds, ax
mov ss, ax
mov bp, 0x9c00
mov sp, bp


mov ah, 0x0e
mov al, 'O'
;int 0x10
mov al, 'k'
;int 0x10

mov bx, KERNEL
mov dh, 2
jmp read
run:
    jmp KERNEL
read:
    mov ah, 0x02
    mov al, dh
    mov ch, 0x00
    mov dh, 0x00
    mov cl, 0x02
    mov dl, [drive] ; drive number
    int 0x13
    jc error
    cmp ah, 0
    jne error
    jmp run
error:
    mov ah, 0x0e
    mov al, 'E'
    int 0x10
    jmp $
print:
    mov ah, 0x0e
    mov al, 'D'
    int 0x10
    ret
drive db 0
times 510-($-$$) db 0
db 0x55, 0xaa
