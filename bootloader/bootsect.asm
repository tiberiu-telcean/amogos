[bits 16]
[org 0x7c00]
cli
KERNEL equ 0x1000
USERSPACE equ 0x9000
%include "kernel.size"
%include "userspace.size"
PIC1 equ 0x20
PIC2 equ 0xA0
PIC1_DATA equ 0x21
PIC2_DATA equ 0xA1
ICW1_ICW4 equ 0x01
ICW1_SINGLE equ 0x02
ICW1_INTERVAL4 equ 0x04
ICW1_LEVEL equ 0x08
ICW1_INIT equ 0x10

ICW4_8086 equ 0x01
ICW4_AUTO equ 0x02
ICW4_BUF_SLAVE equ 0x08
ICW4_BUF_LEVEL equ 0x0C
ICW4_SFNM equ 0x10
;%include "boot.inc"

mov ax, 0xEC00
mov bl, 1
int 0x15 ; tell BIOS our OS is 32 bit

in al, 0x92
or al, 2
out 0x92, al ; enable A20

mov esp, 0x10000 ; temporary stack

mov [drive], dl
mov bx, KERNEL
mov dh, KERNEL_SIZE
mov cl, 3 ; End of boot sector + GDT 
call read

mov bx, USERSPACE
mov dh, USERSPACE_SIZE
mov cl, 3+KERNEL_SIZE
call read
jmp done
read:
    ; bx ; Mem to read into
    mov ah, 0x02 ; Operation ; read
    mov al, dh   ; Lenght
    mov ch, 0x00 ; Head
    mov dh, 0x00 ; Track
    ;cl ; Segment 
    mov dl, [drive] ; drive number
    int 0x13
    jc error
    cmp ah, 0
    jne error
    ret
error:
    mov bx, msg
    dec bx
loop:
    inc bx
    cmp byte [bx], 0
    je  halt
    mov ah, 0x0e
    mov al, byte [bx]
    int 0x10
    jmp loop
drive db 0
done:

mov ebx, 0
install_gdt:
    mov ecx, ebx
    add ecx, gdt
    mov al, byte [ecx]
    mov byte [gdt_pos+ebx], al
    inc ebx
    cmp bx, word [gdt_desc]
    jle install_gdt

    mov ax, word [gdt_desc]
    mov word [gdtd_pos], ax
    mov eax, dword [gdt_desc+2]
    mov dword [gdtd_pos+2], eax
    xor ax, ax
    mov ds, ax
    lgdt [gdtd_pos]

call init_pic

mov eax, cr0 
or eax, 1       ; set PE (Protection Enable) bit in CR0 (Control Register 0)
mov cr0, eax
jmp 08h:clear_pipe

init_pic:
    push bx
    in al, PIC1_DATA
    mov bl, al
    in al, PIC2_DATA
    mov bh, al
    
    mov al, ICW1_INIT
    or al, ICW1_ICW4
    out PIC1, al
    out PIC2, al
    mov al, 0x20 ; offset1
    out PIC1_DATA,al   
    mov al, 0x30 ; offset2
    out PIC2_DATA,al
    mov al, 4
    out PIC1_DATA,al ; tell master there is a slave at IRQ2
    mov al, 2
    out PIC2_DATA,al ; tell slave to connect to IRQ2
    mov al, ICW4_8086
    out PIC1_DATA, al
    out PIC2_DATA, al

    mov al, bl
    out PIC1_DATA, al ; restore masks
    mov al, bh
    out PIC2_DATA, al
    pop bx
    ret
halt:
    hlt
[bits 32]
clear_pipe:
    mov ax, 0x10
    mov ds, ax
    mov ss, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov esp, 0x90000
    mov ebp, esp
    call KERNEL
    cli
    hlt
    jmp $

msg db "Error", 0
;%include "print.inc"
[bits 16]
cli
hlt
times 0x100-($-$$) db 0
%include "gdt.inc"
times 510-($-$$) db 0
db 0x55,0xaa
times 1024-($-$$) db 0
