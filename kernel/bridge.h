/**
 * @file bridge.h
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * C definitions for assembly functions.
 */

#pragma once
#include <stdint.h>

/**
 * Signal to the PIC that the current interrupt is done
 */
extern void eoi(void);
/**
 * Print the current privilege level and returns it.
 */
extern void cpl(void);
extern void init_pic(void);
extern void set_irq(void);
extern void flush_gdt(void *);
extern void flush_tss(void);
extern char _in(short port);
extern void _out(short port, char data);
extern void _set_timer(int freq, int8_t channel);
extern void _cli(void);
extern void _sti(void);
extern void _sleep(uint8_t);