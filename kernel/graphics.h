/**
 * @file graphics.h
 * @author Tiberiu Telcean
 * @version 0.01
 */
#pragma once
#include <stdint.h>

typedef struct attr {
  uint8_t fg : 4;
  uint8_t bg : 4;
} attr_t;

#define WIDTH 80
#define HEIGHT 25

#define BLACK 0x0
#define BLUE 0x1
#define GREEN 0x2
#define CYAN 0x3
#define RED 0x4
#define MAGENTA 0x5
#define BROWN 0x6
#define LIGHTGRAY 0x7
#define DARKGRAY 0x8
#define LIGHTBLUE 0x9
#define LIGHTGREEN 0xa
#define LIGHTCYAN 0xb
#define LIGHTRED 0xc
#define LIGHTMAGENTA 0xd
#define YELLOW 0xe
#define WHITE 0xf

#define SIMPLE                                                                 \
  (attr_t) { .fg = LIGHTGRAY, .bg = BLACK }
#define BRIGHT                                                                 \
  (attr_t) { 0xB, 0x1 }

#define C_H 0x8       /**< Backspace */
#define C_I 0x9       /**< Tab */
#define C_J 0xA       /**< Line feed */
#define C_L 0xC       /**< Form feed */
#define C_M 0xD       /**< Carriage return */
#define C_Escape 0x1B /**< ANSI escape */

typedef struct cell {
  char chr;
  attr_t attr;
} cell;

extern cell *vmem;
extern int x, y;
extern attr_t style;

extern void clear(void);
extern void shift_up(int);
extern void clear_line(int);
extern void in_place(char *message);
extern void print(char *message);
extern void cursor(int x, int y);
extern int t(int xo, int yo);