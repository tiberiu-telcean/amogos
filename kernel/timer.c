#include "timer.h"
#include "atomic.h"
#include <stdint.h>

lock time_lock;
float time = 0;

void update_time(void) {
  atom_lock(&time_lock);
  atom_add(&time_lock, (union atom){.f = 1.0 / (TIMER_P)});
  atom_unlock(&time_lock);
}

void init_time(void) {
  time_lock.locked = false;
  time_lock.type = F;
  time_lock.atom = &(union atom){.f = time};
}

float get_time(void) {
  float ret;
  atom_unlock(&time_lock);
  ret = atom_get(&time_lock).f;
  atom_lock(&time_lock);
  return ret;
}