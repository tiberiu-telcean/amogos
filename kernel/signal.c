#include "signal.h"
#include <stdbool.h>
#include "userspace.h"

int signal(int sig, task *receiver, regs *task_state) {
  switch (sig) {
  case (SIGSTOP):
    receiver->stopped = true;
    scheduler(task_state);
    break;
  case (SIGCONT):
    receiver->stopped = false;
    break;
  case(SIGKILL):
    exit_task(receiver, task_state);
    return 1;
    break;
  }
  return 0;
}