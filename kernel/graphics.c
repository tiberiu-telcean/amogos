/**
 * @file graphics.c
 * @author Tiberiu Telcean
 * @version 0.01
 * @section description
 * Modify video memory.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "graphics.h"
#include "bridge.h"
#include <stdbool.h>
#include "libk/stdlib.h"
#include "libk/string.h"

cell *vmem = (cell *)0xb8000;
int x, y;
attr_t style = SIMPLE;

void clear(void) {
  for (int i = 0; i < WIDTH; i++) {
    for (int j = 0; j < HEIGHT; j++) {
      vmem[t(i, j)] = (cell){0, 0};
    }
  }
}

void clear_line(int l) {
  for (int i = 0; i < WIDTH; i++) {
    vmem[t(i, l)] = (cell){0, 0};
  }
}

/**
 * Use I.O to set the cursor. This should be done using control characters in
 * tty.c
 * @param x
 * @param y
 * @see tty.c
 */
void cursor(int x, int y) {
  uint16_t pos = y * WIDTH + x;
  _out(0x3d4, 0x0F);
  _out(0x3d5, (uint8_t)(pos & 0xFF));
  _out(0x3d4, 0x0E);
  _out(0x3d5, (uint8_t)((pos >> 8) & 0xFF));
}

void in_place(char *message) {
  clear_line(y);
  print(message);
  x = 0;
}

/**
 * Put a string in video memory.
 * @param message string
 * @param style the properties of the text.
 */
void print(char *message) {
  int i, temp_x, temp_y;
  bool fe_escape = false, control_sequence = false;
  int n, m, r, g, b;
  int8_t arg_no;
  for (i = 0; message[i] != 0; i++) {
    if (fe_escape && !control_sequence && message[i] == '[') {
      n = m = r = g = b = 0;
      control_sequence = true;
      continue;
    }
    if (control_sequence) {
      if ('0' <= message[i] && message[i] <= '9') {
        switch (arg_no) {
        case (0):
          n *= 10;
          n += message[i] - '0';
          break;
        case (1):
          m *= 10;
          m += message[i] - '0';
          break;
        case (2):
          r *= 10;
          r += message[i] - '0';
          break;
        case (3):
          g *= 10;
          g += message[i] - '0';
          break;
        case (4):
          b *= 10;
          b += message[i] - '0';
          break;
        }
        continue;
      }
      if (message[i] == ';') {
        arg_no++;
        continue;
      }
      if (0x40 <= message[i] && message[i] <= 0x7E) {
        switch (message[i]) {
        case 'A': // CUU
          y -= n;
          if (y < 0)
            y = 0;
          break;
        case 'B': // CUD
          y += n;
          if (y > HEIGHT)
            y = HEIGHT;
          break;
        case 'C': // CUF
          x += n;
          if (x > WIDTH)
            x = WIDTH;
          break;
        case 'D': // CUB
          x -= n;
          if (x < 0)
            x = 0;
          break;
        case 'E': // CNL
          y += n;
          if (y > HEIGHT)
            y = HEIGHT;
          x = 0;
          break;
        case 'F': // CPL
          y -= n;
          if (y < 0)
            y = 0;
          x = 0;
          break;
        case 'G': // CHA
          y = n - 1;
          break;
        case 'H': // CUP
          x = n - 1;
          y = m - 1;
          break;
        case 'J': // ED
          switch (n) {
          case (0):
            for (temp_y = y; y < HEIGHT; y++) {
              clear_line(y);
            }
            y = temp_y;
            break;
          case (1):
            for (temp_y = y; y >= 0; y--) {
              clear_line(y);
            }
            y = temp_y;
            break;
          case (2):
            clear();
            break;
          }
          break;
        case 'K': // EL
          switch (n) {
          case (0):
            for (int i = y; i < WIDTH; i++) {
              vmem[t(i, y)] = (cell){0, 0};
            }
            break;
          case (1):
            for (int i = y; i >= 0; i--) {
              vmem[t(i, y)] = (cell){0, 0};
            }
            break;
          case (2):
            clear_line(y);
            break;
          }
          break;
        case 'S': // SU
        case 'T': // SD
          break;
        case 'm': // SGR
          switch (n) {
          case (0):
            style = SIMPLE;
            break;
          case (38):
            if (m == 5) {
              switch (r) {
              case (30):
                style.fg = BLACK;
                break;
              case (31):
                style.fg = RED;
                break;
              case (32):
                style.fg = GREEN;
                break;
              case (33):
                style.fg = YELLOW;
                break;
              case (34):
                style.fg = BLUE;
                break;
              case (35):
                style.fg = MAGENTA;
                break;
              case (36):
                style.fg = CYAN;
                break;
              case (37):
                style.fg = LIGHTGRAY;
                break;
              case (90):
                style.fg = DARKGRAY;
                break;
              case (91):
                style.fg = LIGHTRED;
                break;
              case (92):
                style.fg = LIGHTGREEN;
                break;
              case (93):
                style.fg = YELLOW;
                break;
              case (94):
                style.fg = LIGHTBLUE;
                break;
              case (95):
                style.fg = LIGHTMAGENTA;
                break;
              case (96):
                style.fg = LIGHTCYAN;
                break;
              case (97):
                style.fg = WHITE;
                break;
              }
            }
            break;
          case (39):
            style.fg = SIMPLE.fg;
            break;
          case (48):
            if (m == 5) {
              switch (r) {
              case (40):
                style.bg = BLACK;
                break;
              case (41):
                style.bg = RED;
                break;
              case (42):
                style.bg = GREEN;
                break;
              case (43):
                style.bg = YELLOW;
                break;
              case (44):
                style.bg = BLUE;
                break;
              case (45):
                style.bg = MAGENTA;
                break;
              case (46):
                style.bg = CYAN;
                break;
              case (47):
                style.bg = LIGHTGRAY;
                break;
              case (100):
                style.bg = DARKGRAY;
                break;
              case (101):
                style.bg = LIGHTRED;
                break;
              case (102):
                style.bg = LIGHTGREEN;
                break;
              case (103):
                style.bg = YELLOW;
                break;
              case (104):
                style.bg = LIGHTBLUE;
                break;
              case (105):
                style.bg = LIGHTMAGENTA;
                break;
              case (106):
                style.bg = LIGHTCYAN;
                break;
              case (107):
                style.bg = WHITE;
                break;
              }
            }
            break;
          }
          break;
        case (49):
          style.bg = SIMPLE.bg;
          break;
        default:
          break;
        }
        fe_escape = control_sequence = false;
        continue;
      }
    }
    switch (message[i]) {
    case C_H:
      if (x == 0) {
        x = WIDTH;
        y--;
      } else
        x--;
      break;
    case C_I:
      x -= x % 4;
      x++;
      break;
    case C_J:
      y++;
      x = 0;
      break;
    case C_L:
      break;
    case C_M:
      x = 0;
      break;
    case C_Escape:
      fe_escape = true;
      break;
    default:
      vmem[t(x, y)] = (cell){message[i], style};
      if (x == WIDTH) {
        x = 0;
        y++;
      } else
        x++;
      if (y == HEIGHT) {
        shift_up(1);
        y--;
      }
    }
  }
}

void shift_up(int n) {
  for (int i = 0; i < HEIGHT - 1; i++)
    for (int j = 0; j < WIDTH; j++)
      vmem[t(j, i)] = vmem[t(j, i + 1)];
  clear_line(HEIGHT - 1);
}

int t(int xo, int yo) { return (yo + xo / WIDTH) * WIDTH + xo % WIDTH; }
