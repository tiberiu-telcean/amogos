/**
 * @file main.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Entry point of the kernel. It initializes some IA-32 tables, creates init
 * task and enters into user mode.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "bridge.h"
#include "config.h"
#include "graphics.h"
#include "keyboard.h"
#include "libk/log.h"
#include "libk/stdlib.h"
#include "mdev.h"
#include "paging.h"
#include "sound.h"
#include "timer.h"
#include "tty.h"
#include "userspace.h"

typedef void (*driver_entry)(void);
gdt_entry *gdt = (gdt_entry *)0x10000;

#ifdef MODULES
// clang-format off
driver_entry modules[] = {vfs_main,
                          mdev_main,
                          tty_main,
                          #ifdef SOUND
                          sound_module,
                          #endif
                          NULL};
// clang-format on
#endif
extern int user(void);
void _start() {
  task *init;
#ifdef MULTITASKING
  task *idle;
#endif
  clear();
  init_time();
  _set_timer(1000, 0);
#ifdef RTC
  _cli();
  _out(0x70, 0x8B);
  char p = _in(0x71);
  _out(0x70, 0x8B);
  _out(0x71, p | 0x40); // set RTC
#endif
  status("Set timers");
  set_irq();
  status("Set interrupts");

  set_tss(gdt + 5);
  status("Set TSS");

#ifdef PAGING
  set_paging();
  status("Set paging");
#endif

#ifdef MODULES
  for (int i = 0; modules[i] != NULL; i++) {
    modules[i]();
  }
#endif
  status("Running");
  if (key_buffer == NULL) {
    key_buffer = (key *)malloc(sizeof(key) * 2);
  }
  init = new_task(0x50000, 3);
#ifdef MULTITASKING
  idle = new_task(idler, 3);
  go_to(init);
#endif
  usermode(0x50000);
}

int idler(void) {
  for (;;)
    ;
}

/** @mainpage
 * features
 * * Multitasking
 * * VFS
 */