#pragma once
#include "user.h"
#include <stdbool.h>
#include <stdint.h>

typedef uint32_t (*io_op)(void *, int32_t);

typedef struct perm {
  bool r : 1;
  bool w : 1;
  bool x : 1;
} perm;

typedef struct file {
  user_t owner;
  perm owner_perm;
  perm reg_perm;
  char *name;
  struct dir *parent;
  io_op write;
  io_op read;
  bool mode : 1;
} file;

typedef struct dir {
  user_t owner;
  perm owner_perm;
  perm reg_perm;
  char *name;
  struct dir *parent;
  int8_t n_children;
  struct node *children;
} dir;

typedef struct node {
  bool type : 1; /**< 0:file 1:dir */
  struct file *f;
  struct dir *d;
} node;
#define TYPE_FILE false
#define TYPE_DIR true

#define SIZE_CHILDREN 16

extern dir *root_d;

extern void add_child(dir *parent, node *child);
extern node *open_file(char *path, char *mode);
extern void vfs_main(void);