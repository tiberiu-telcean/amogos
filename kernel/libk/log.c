#include "./log.h"
#include "../graphics.h"
#include "../timer.h"
#include "./stdlib.h"
#include "./string.h"
#include <stdarg.h>
#include <stdbool.h>

void printk(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  char buf[512], temp_s[512];
  char *temp_c;
  int temp_i, iter;
  bool in_format = false;
  uint32_t phys_index = 0;
  memset(buf, 0, 512);
  for (int i = 0; fmt[i] != 0; i++) {
    memset(temp_s, 0, 512);
    if (in_format) {
      switch (fmt[i]) {
      case 'd':
        temp_i = va_arg(ap, int);
        if (temp_i == 0) {
          temp_s[10] = '0';
          iter = 1;
          goto d_skip;
        }
        iter = 0;
        while (temp_i) {
          temp_s[10 - iter] = temp_i % 10 + '0';
          iter++;
          temp_i /= 10;
        }
      d_skip:
        strcpy(buf + phys_index, temp_s + (11 - iter));
        phys_index += iter;
        in_format = false;
        break;
      case 's':
        temp_c = va_arg(ap, char *);
        strcpy(&buf[phys_index], temp_c);
        phys_index += strlen(temp_c);
        in_format = false;
        break;
      }
      continue;
    }
    switch (fmt[i]) {
    case '%':
      in_format = true;
      break;
    default:
      buf[phys_index] = fmt[i];
      phys_index++;
      break;
    }
  }
  print(buf);
  va_end(ap);
}

void status(char *message) { printk("*[%d] %s\n", (int)get_time(), message); }