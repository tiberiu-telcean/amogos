#include "stdlib.h"
#include "../userspace.h"
#include "log.h"
#include "syscall.h"
#include <stdnoreturn.h>

void noreturn quick_exit(int status) {
  csc(SYS_EXIT, status, NUSED, NUSED);
  for (;;)
    ;
  asm("hlt");
};

void noreturn abort(void) {
  status("PANIC");
  asm("cli");
  for (;;)
    asm("hlt");
}

int system(int32_t (*eip)(void)) {
  uint32_t cr_ring;
  asm("mov %0, cs" : "=a"(cr_ring));
  new_task(eip, cr_ring & 0b11);
  return 0;
}

void exit(int status) { quick_exit(status); }