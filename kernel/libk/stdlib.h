#pragma once
#include <stddef.h>
#include <stdint.h>

extern void quick_exit(int status);
extern void exit(int status);
extern void free(void *link);
extern int system(int32_t (*)(void));
extern void *malloc(size_t _size);