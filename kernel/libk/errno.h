#pragma once

static volatile int __ERRNO_INT_ERROR = 0;

#define errno __ERRNO_INT_ERROR

#define EINVAL 2
#define EACCES 3
#define EPERM 4
#define EDOM 5
#define EAGAIN 6
#define EILSEQ 7
#define EIO 30
#define ENOTTY 31
#define EBADF 32
#define ENOENT 33
#define EISDIR 34

#define N_ERRORS 5