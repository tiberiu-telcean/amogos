/**
 * @file stdio.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Writing and reading from streams
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "./stdio.h"
#include "./errno.h"
#include "./string.h"
#include <stdarg.h>
#include <stdbool.h>
#include <stdint.h>
#include <syscall.h>

FILE *stdin = &(FILE){.fd = 0, .error = 0};
FILE *stdout = &(FILE){.fd = 1, .error = 0};
FILE *stderr = &(FILE){.fd = 2, .error = 0};

size_t fwrite(void *buf, size_t size, size_t count, FILE *stream) {
  size_t wrote;
  wrote =
      csc(SYS_WRITE, (uint32_t)buf, size * count, (uint32_t)stream->fd) / size;
  return wrote;
}

uint32_t puts(char *buf) {
  uint32_t ret;
  ret = fwrite(buf, 1, strlen(buf), stdout);
  ret = fwrite("\n", 1, 1, stdout);
  return ret;
}

int putc(int c, FILE *stream) { return fputc(c, stream); }

int fputc(int c, FILE *stream) {
  char buf[1];
  buf[0] = (unsigned char)c;
  stream->error = fwrite(buf, 1, 1, stream);
  return stream->error ? c : EOF;
}

int ferror(FILE *stream) { return stream->error; }

int putchar(int c) { return fputc(c, stdout); }

void fprintf(FILE *stream, char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stream, fmt, ap);
  va_end(ap);
}

void vfprintf(FILE *stream, char *fmt, va_list ap) {
  char buf[512], temp_s[512];
  int temp_i, iter;
  bool in_format = false;
  uint32_t phys_index = 0;
  memset(buf, 0, 512);
  for (int i = 0; fmt[i] != 0; i++) {
    memset(temp_s, 0, 512);
    if (in_format) {
      switch (fmt[i]) {
      case 'd':
        temp_i = va_arg(ap, int);
        if (temp_i == 0) {
          temp_s[10] = '0';
          iter = 1;
          goto d_skip;
        }
        iter = 0;
        while (temp_i) {
          temp_s[10 - iter] = temp_i % 10 + '0';
          iter++;
          temp_i /= 10;
        }
      d_skip:
        strcpy(buf + phys_index, temp_s + (11 - iter));
        phys_index += iter;
        in_format = false;
        break;
      }
      continue;
    }
    switch (fmt[i]) {
    case '%':
      in_format = true;
      break;
    default:
      buf[phys_index] = fmt[i];
      phys_index++;
      break;
    }
  }
  fwrite(buf, 1, phys_index + 1, stream);
  return;
}

void vprintf(char *fmt, va_list ap) { vfprintf(stdout, fmt, ap); }

void printf(char *fmt, ...) {
  va_list ap;
  va_start(ap, fmt);
  vprintf(fmt, ap);
  va_end(ap);
}

char *msg[N_ERRORS];

void perror(const char *msg) {}
