#pragma once
#include <stdarg.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

typedef int16_t fd_t;

typedef struct FILE {
  fd_t fd;
  bool error : 1;
  bool rw : 1;
  bool eof : 1;
} FILE;

#define EOF -1

extern FILE *stdin;
extern FILE *stdout;
extern FILE *stderr;

extern uint32_t puts(char *buf);
extern size_t fwrite(void *buf, size_t size, size_t count, FILE *stream);
extern void vfprintf(FILE *stream, char *fmt, va_list ap);
extern void vprintf(char *fmt, va_list ap);
extern void fprintf(FILE *stream, char *fmt, ...);
extern void printf(char *fmt, ...);
extern int ferror(FILE *stream);
extern int putc(int c, FILE *stream);
extern int fputc(int c, FILE *stream);
extern int putchar(int c);
extern void perror(const char *msg);