; Copyright (c) 2021 Tiberiu Telcean
;
; Permission is hereby granted, free of charge, to any person obtaining a copy
; of this software and associated documentation files (the "Software"), to deal
; in the Software without restriction, including without limitation the rights
; to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
; copies of the Software, and to permit persons to whom the Software is
; furnished to do so, subject to the following conditions:
;
; The above copyright notice and this permission notice shall be included in
; all copies or substantial portions of the Software.
;
; THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
; FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
; AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
; LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
; OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
; SOFTWARE.
[bits 32]
section .data
    timer: dd 0
    msg: db "Hello, in kernel, CPL:",0
    GPF: db "GPF", 0
    extern scancode
    extern prev
    %include "idt.inc"
section .text
    GLOBAL _sleep
    GLOBAL timer_irq
    GLOBAL ps2_irq
    GLOBAL usermode
    GLOBAL nop_irq
    GLOBAL set_irq
    GLOBAL init_pic
    GLOBAL cpl
    GLOBAL eoi
    GLOBAL flush_tss
    GLOBAL flush_gdt
    GLOBAL flush_tlb
    GLOBAL _sti
    GLOBAL _cli
    GLOBAL csc

    EXTERN ps2_driver
    EXTERN status
    EXTERN itoa
    EXTERN itoh

    EXTERN user
    EXTERN idler
    GLOBAL syscall_handler
    EXTERN syscall_select
    EXTERN scheduler
    EXTERN switch_task
set_irq:
    cli
    push eax
    push esi
    push ebx
    mov ebx, 0
    mov esi, 0x20000
irq_loop:
    cmp ebx, 0x20
    je def_timer
    cmp ebx, 0x21
    je def_keyboard
    cmp ebx, 0x0D
    je def_gpf
    cmp ebx, 0x88
    je def_syscall
    jmp def_nop
irq_continue:
    add esi, 0x08
    inc ebx
    cmp ebx, 0x100
    jne irq_loop
    pop ebx
    pop esi
    pop eax
    lidt [idtr]
    ret
def_nop:
    mov eax, nop_irq
    mov word [esi], ax
    mov word [esi+2], 8h
    mov byte [esi+4], 0x00
    shr eax, 16
    mov byte [esi+5], 0b00001110
    mov word [esi+6], ax
    jmp irq_continue
def_gpf:
    mov eax, gpf
    mov word [esi], ax
    mov word [esi+2], 8h
    mov byte [esi+4], 0x00
    shr eax, 16
    mov byte [esi+5], 0b10001110
    mov word [esi+6], ax
    jmp irq_continue
def_syscall:
    mov eax, syscall_handler
    mov word [esi], ax ; offset 0-15
    mov word [esi+2], 8h ; selector
    mov byte [esi+4], 0x00 ; zero
    mov byte [esi+5], 0b11101110 ; type
    shr eax, 16
    mov word [esi+6], ax ; offset 16-31
    jmp irq_continue
gpf:
    pusha
    push GPF
    call status
    pop eax
    call eoi
    call cpl
    jmp $
    popa
    iret
def_timer:
    mov eax, timer_irq
    mov word [esi], ax
    mov word [esi+2], 8h
    mov byte [esi+4], 0x00
    shr eax, 16
    mov byte [esi+5], 0b10001110
    mov word [esi+6], ax
    jmp irq_continue
def_keyboard:
    mov eax, ps2_irq
    mov word [esi], ax
    mov word [esi+2], 8h
    mov byte [esi+4], 0x00
    shr eax, 16
    mov byte [esi+5], 0b10001110
    mov word [esi+6], ax
    jmp irq_continue
ps2_irq:
    cli
    pusha
    call ps2_driver
    call eoi
    popa
    sti
    iret
syscall_handler:
    cli
    pusha
    mov eax, 0x0
    push eax
    mov eax, 0x88
    push eax ; push isr info
    xor eax,eax
    mov ax, ds   ; store ring3 selector
    push eax

    mov ax, 0x10 ; switch to ring0 selectors
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    push esp
    call syscall_select
    add esp, 4

    pop ebx
    mov ds, bx
    mov es, bx
    mov fs, bx
    mov gs, bx
    add esp, 8
    popa
    sti
    iret
timer_irq:
    cli
    pusha
    mov eax, 0x0
    push eax
    mov eax, 0x20
    push eax
    xor eax, eax
    mov ax, ds
    push eax
    mov ax, 0x10
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    push esp
    call scheduler
    add esp, 4
    mov eax, [timer]
    or eax, eax
    jz timer_done
    dec eax
    mov [timer], eax
timer_done:
    call eoi
    pop eax
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax
    add esp, 8
    popa
    sti
    iret
eoi:
    mov al, 20h
    out 20h, al
    ret
nop_irq:
    nop
    call eoi
    iret
_sleep:
    push eax
    mov eax, [esp+8]
    mov [timer], eax
sleep_loop:
    cli
    mov eax, [timer]
    or eax, eax
    jz sleep_done
    sti
    times 50 nop
    jmp sleep_loop
sleep_done:
    sti
    pop eax
    ret
usermode:
    cli
    mov ebp, esp
    mov ax, (4*8) | 3
    mov ds, ax
    mov es, ax
    mov fs, ax
    mov gs, ax

    push (4*8) | 3
    mov eax, 0x60000 ; user stack
    push eax
    pushf
    pop eax
    or eax, 0x202
    push eax
    push (3*8) | 3
    push dword [ebp+4] ; entry point
    ; alternative way of context switching using new intel instructions
    ; xor edx, edx
    ; mov eax, 0x100008
    ; mov ecx, 0x174
    ; wrmsr

    ; mov edx, user
    ; mov ecx, 0x60000 ; user stack
    sti
    ; sysexit
    iret
flush_gdt:
    lgdt [0x0fff0]
    mov ax, 0x10      ; 0x10 is the offset in the GDT to our data segment
    mov ds, ax        ; Load all data segment selectors
    mov es, ax
    mov fs, ax
    mov gs, ax
    mov ss, ax
    jmp 0x08:.flush
.flush:
    ret
flush_tss:
    push eax
    mov ax, (5*8)|3
    ltr ax
    pop eax
    ret
cpl:
    push eax

    xor eax, eax
    mov ax, cs
    and eax, 3
    push eax
    call itoa
    push eax
    call status
    pop eax
    pop eax

    pop eax
    ret
;; move a pointer (or flush) to cr3
flush_tlb:
    push ebp
    mov ebp, esp
    push eax
    mov eax, [ebp+4]
    jz .read
.write:
    mov cr3, eax
    pop eax
    pop ebp
    ret
.read:
    mov eax, cr3
    jmp .write

_sti:
    sti
    ret
_cli:
    cli
    ret
csc:
    push ebp
    mov ebp, esp
    pusha
    mov eax, [ebp+0x8]
    mov ebx, [ebp+0xc]
    mov ecx, [ebp+0x10]
    mov edx, [ebp+0x14]
    int 0x88
    mov [ebp-4], eax
    popa
    pop ebp
    ret
