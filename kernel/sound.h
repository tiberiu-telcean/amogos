#pragma once
#include <stdbool.h>
#include <stdint.h>

typedef struct note {
  int duration;
  int frequency;
  bool present : 1;
  bool played : 1;
} note;

extern note soundbuffer[];

extern void sound_module(void);
extern void sound_update(void);
extern void _set_timer(int freq, int8_t channel);
extern void add_note(struct note *);