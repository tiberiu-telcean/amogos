#pragma once
#include <stddef.h>
#include <stdint.h>
#include "userspace.h"

struct tty {
  int8_t num : 3;
};

extern file *tty_driver;

extern void tty_main(void);
extern uint32_t tty_send(void *, int32_t);
extern uint32_t tty_receive(void *, int32_t);
