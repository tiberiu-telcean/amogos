/**
 * @file vfs.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Keep track of files.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "vfs.h"
#include "errno.h"
#include "log.h"
#include "stdlib.h"
#include "string.h"
#include "user.h"

dir *root_d;
void vfs_main(void) {
  root_d = (dir *)malloc(sizeof(dir));
  memset(root_d, 0, sizeof(dir));
  root_d->name = "";
  root_d->parent = NULL;
  root_d->owner = root;
  root_d->owner_perm = (perm){true, true, true};
  root_d->reg_perm = (perm){true, false, true};
  root_d->children = (node *)malloc(sizeof(node) * SIZE_CHILDREN);
  status("Initialized VFS");
}

node *open_file(char *path, char *mode) {
  char *name = strtok(path, "/");
  dir *cur_dir = root_d;
  while (name) {
    for (int child = 0; (cur_dir->children[child].f != NULL ||
                         cur_dir->children[child].d != NULL) &&
                        child < SIZE_CHILDREN;
         child++) {
      if (cur_dir->children[child].type) {
        if (!strcmp(cur_dir->children[child].d->name, name)) {
          cur_dir = cur_dir->children[child].d;
        }
      } else {
        if (!strcmp(cur_dir->children[child].f->name, name)) {
          return &cur_dir->children[child];
        }
      }
    }
    name = strtok(NULL, "/");
  }
  errno = -ENOENT;
  return 0;
}

void add_child(dir *parent, node *child) {
  if (parent->n_children < SIZE_CHILDREN) {
    memcpy(&parent->children[parent->n_children], child, sizeof(node));
    parent->n_children++;
  }
}