#include "bridge.h"
#include <stdint.h>

char inline _in(short port) {
  unsigned char result;
  __asm__ volatile("in %0, %1" : "=a"(result) : "Nd"(port));
  return result;
}

void inline _out(short port, char data) {
  __asm__("out %1, %0" : : "a"(data), "Nd"(port));
}

void _set_timer(int freq, int8_t channel) {
  uint32_t temp = 1193180 / freq;
  _cli();
  _out(0x43, 0b10110100 | channel % 4);
  _out(0x40 | channel % 4, (uint8_t)temp & 0xFF);
  _out(0x40 | channel % 4, (uint8_t)(temp >> 8) & 0xFF);
  _sti();
}

int keys(void) { return (int)_in(0x60); }
