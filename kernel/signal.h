#pragma once
#include "userspace.h"

#define SIGHUP 1
#define SIGSTOP 2
#define SIGCONT 3
#define SIGINT 4
#define SIGKILL 5

int signal(int sig, task *receiver, regs *task_state);