/**
 * @file mdev.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Filesystem for managing drivers.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "mdev.h"
#include "libk/stdlib.h"
#include "vfs.h"

dir *mdev;
void mdev_main(void) {
  mdev = (dir *)malloc(sizeof(dir));
  mdev->name = "dev";
  mdev->parent = root_d;
  mdev->owner = root;
  mdev->owner_perm = (perm){true, true, true};
  mdev->reg_perm = (perm){true, false, true};
  mdev->children = (node *)malloc(sizeof(node) * SIZE_CHILDREN);
  node *mdev_node = (node *)malloc(sizeof(node));
  mdev_node->type = TYPE_DIR;
  mdev_node->d = mdev;
  add_child(root_d, mdev_node);
}