#pragma once
#include <stdbool.h>
#include <stdint.h>

typedef uint8_t keycode_t;

typedef struct key {
  keycode_t prefix;
  keycode_t scancode;
} key;

struct __attribute__((__packed__)) key_bitmask {
  bool A : 1;
  bool B : 1;
  bool C : 1;
  bool D : 1;
  bool E : 1;
  bool F : 1;
  bool G : 1;
  bool H : 1;
  bool I : 1;
  bool J : 1;
  bool K : 1;
  bool L : 1;
  bool M : 1;
  bool N : 1;
  bool O : 1;
  bool P : 1;
  bool Q : 1;
  bool R : 1;
  bool S : 1;
  bool T : 1;
  bool U : 1;
  bool V : 1;
  bool W : 1;
  bool X : 1;
  bool Y : 1;
  bool Z : 1;
  bool space : 1;
  bool left : 1;
  bool down : 1;
  bool up : 1;
  bool right : 1;
};

extern key *key_buffer;
extern void ps2_driver(void);

extern char *table;
