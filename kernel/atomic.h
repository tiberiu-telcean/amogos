#pragma once
#include <stdbool.h>
#include <stdint.h>

typedef struct lock {
  union atom {
    int32_t i32;
    int16_t i16;
    int8_t i8;
    float f;
  } * atom;
  enum type { I32, I16, I8, F } type;
  bool locked;
} lock;

void atom_lock(lock *);
void atom_unlock(lock *);
void atom_add(lock *, union atom);
union atom atom_get(lock *);