#include "atomic.h"
#include <stdbool.h>

void atom_lock(lock *l) { l->locked = true; }

void atom_unlock(lock *l) { l->locked = false; }

void atom_add(lock *l, union atom o) {
  if (l->locked) {
    return;
  }
  switch (l->type) {
  case (I32):
    l->atom->i32 += o.i32;
    break;
  case (I16):
    l->atom->i16 += o.i16;
    break;
  case (I8):
    l->atom->i8 += o.i8;
    break;
  case (F):
    l->atom->f += o.f;
    break;
  }
}

union atom atom_get(lock *l) {
  if (l->locked)
    return (union atom){0};
  return *l->atom;
}