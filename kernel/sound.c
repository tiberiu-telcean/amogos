#include "sound.h"
#include "bridge.h"
#include "graphics.h"
#include "libk/string.h"
#include <stdbool.h>

note soundbuffer[128];

void sound_module(void) { memset(soundbuffer, 0, sizeof(note) * 128); }
int timer = 0;
int playing = 0;
int writing = 0;
void sound_update(void) {
  if (timer) {
    timer--;
    return;
  }
  if (playing >= writing) {
    playing = writing;
    return;
  }
  if (soundbuffer[playing % 128].present &&
      !soundbuffer[playing % 128].played) {
    _set_timer(soundbuffer[playing % 128].frequency, 2);
    _out(0x61, _in(0x61) | 3);
    timer = soundbuffer[playing % 128].duration;
    soundbuffer[playing % 128].played = true;
  } else if (soundbuffer[playing % 128].present &&
             soundbuffer[playing % 128].played) {
    _out(0x61, _in(0x61) & 0xFC);
    playing++;
  }
}

void add_note(note *n) {
  if (!soundbuffer[writing % 128].present) {
    memcpy(&soundbuffer[writing % 128], n, sizeof(note));
    writing++;
  }
}