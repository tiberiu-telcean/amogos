/**
 * @author Tiberiu Telcean
 * @version 0.01
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "keyboard.h"
#include "bridge.h"
#include "libk/stdlib.h"
#include "libk/string.h"

key *key_buffer = NULL;

/**
 * Handle an interrupt from the keyboard.
 * When a key is pressed, a scancode is read into the buffer, if the byte read
 * is the start of a multi-byte command, the scancode is moved into prefix. At
 * the end, the buffer is shifted to the left.
 */
void ps2_driver(void) {
  key *cur;
  if (key_buffer == NULL) {
    key_buffer = (key *)malloc(sizeof(key) * 2);
  }
start:
  cur = &key_buffer[1];
  if (cur->prefix == 0) { // no key
    __asm__ volatile("in %0, %1" : "=a"(cur->prefix) : "Nd"(0x60));
    if (cur->prefix != 0xE0) { // single byte
      cur->scancode = cur->prefix;
    }
  } else if (cur->scancode == 0) { // first half of key only
    if (cur->prefix == 0xE0) {     // multi-byte
      __asm__ volatile("in %0, %1" : "=a"(cur->scancode) : "Nd"(0x60));
    }
  } else { // key filled
    memcpy(key_buffer, &key_buffer[1], sizeof(key));
    memset(&key_buffer[1], 0, sizeof(key));
    goto start;
  }
  return;
};

char *table = "0?1234567890-=??"
              "QWERTYUIOP[]??AS"
              "DFGHJKL;'`?\\ZXCV"
              "BNM,./?*? ??????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "0?1234567890-=??"
              "QWERTYUIOP[]??AS"
              "DFGHJKL;'`?\\ZXCV"
              "BNM,./?*? ??????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????"
              "????????????????";
