#pragma once
#include <stdbool.h>
#include <stdint.h>

typedef struct __attribute__((__packed__)) {
  bool present : 1;
  bool rw : 1;           /**< 0:read 1:read/write */
  bool user : 1;         /**< 0:supervizor 1:all */
  bool writethrough : 1; /**< 0:write-back 1:write-through */
  bool cached : 1;       /**< 0:cache 1:disable */
  bool accessed : 1;
  bool dirty : 1; /**< This page has been written to. */
  bool zero : 1;
  bool global : 1; /**< Prevent TLB from updating its cache */
  int8_t available : 3;
  uint32_t address : 20; /**< 4KB aligned */
} page_table_entry;

typedef page_table_entry* page_table;

typedef struct __attribute__((__packed__)) {
  bool present : 1;
  bool rw : 1;           /**< 0:read 1:read/write */
  bool user : 1;         /**< 0:supervizor 1:all */
  bool writethrough : 1; /**< 0:write-back 1:write-through */
  bool cached : 1;       /**< 0:cache 1:disable */
  bool accessed : 1;
  bool zero : 1;
  bool pagesize : 1; /**< 0:4KiB 1:4MiB */
  bool ignored : 1;
  int8_t available : 3;
  uint32_t address : 20; /**< 4KB aligned */
} page_dir_entry;

typedef page_dir_entry* page_directory;

extern void flush_tlb(page_directory);
extern void set_paging(void);
