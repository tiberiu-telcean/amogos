/**
 * @file tty.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Basic interface between user and program.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
#include "tty.h"
#include "bridge.h"
#include "graphics.h"
#include "keyboard.h"
#include "libk/errno.h"
#include "libk/log.h"
#include "libk/stdlib.h"
#include "mdev.h"
#include "user.h"
#include "userspace.h"

file *tty_driver;
void tty_main(void) {
  tty_driver = (file *)malloc(sizeof(file));
  tty_driver->read = tty_receive;
  tty_driver->write = tty_send;
  tty_driver->name = "tty";
  tty_driver->owner = root;
  tty_driver->owner_perm = (perm){true, true, false};
  tty_driver->reg_perm = (perm){true, true, false};
  tty_driver->parent = mdev;
  node *tty_node = (node *)malloc(sizeof(node));
  tty_node->type = TYPE_FILE;
  tty_node->f = tty_driver;
  add_child(mdev, tty_node);
  status("Initialized TTY");
}

/**
 * Write a string to video memory.
 * @param buf string
 * @param size number of bytes to write.
 * @see print
 */
uint32_t tty_send(void *buf, int32_t size) {
  if (buf != NULL) {
    print((char *)buf);
  } else {
    return -EINVAL;
  }
  return size;
}

/**
 * Read a string from the keyboard.
 * @param buf string
 * @param size number of bytes to wait for.
 */
uint32_t tty_receive(void *buf, int32_t size) {
  for (int i = 0; i < size; i++) {
    _cli();
    if (key_buffer[1].prefix == 0 && key_buffer[1].scancode != 0) { // character
      ((char *)buf)[i] = table[key_buffer[1].scancode];
      continue;
    }
    _sti();
  }
  return (uint32_t)buf;
}
