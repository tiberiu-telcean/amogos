/**
 * @file userspace.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Dealing with multiple processes and rings.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "userspace.h"
#include "bridge.h"
#include "config.h"
#include "errno.h"
#include "signal.h"
#include "sound.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "timer.h"
#include "tty.h"
#include "vfs.h"
#include <stddef.h>
#include <syscall.h>

#define ZERO 0

extern gdt_entry *gdt;
tss_entry tss0;
task *first_task;
task *last_task;
task *current_task;
uint16_t last_pid = 0;

/**
 * Initialize the task state segment for jumping between rings.
 * @param g Pointer to GDT entry with which it will be installed.
 */
void set_tss(gdt_entry *g) {
  uint32_t base = (uint32_t)(&tss0);
  uint32_t limit = base + sizeof(tss_entry);
  g->limit_low = limit;
  g->base_low = base;
  g->base_middle = (base & (0xff << 16)) >> 16;
  g->base_high = (base & (0xff << 24)) >> 24;
  g->limit_high = (limit & (0xf << 16)) >> 16;
  g->access = 0xE9;
  g->granularity = 0x0;
  memset(&tss0, 0, sizeof(tss_entry));

  tss0.ss0 = 0x10;
  tss0.esp0 = (uint32_t)malloc(0x5000);
  tss0.cs = 0x0b;
  tss0.ss = tss0.ds = tss0.es = tss0.fs = tss0.gs = 0x23;
  tss0.iomap_base = sizeof(tss_entry);

  flush_gdt(gdt);
  flush_tss();
}

#define F_INT 0x0200
#define F_RES 0x0002

/**
 * Create a new task.
 * @param eip Code to jump to when the task is scheduled.
 * @param ring Privilege level.
 */
task *new_task(int(eip)(void), uint8_t ring) {
  task *new = (task *)malloc(sizeof(task));
  node *filearr = (node *)malloc(sizeof(node) * 256);
  memset(filearr, 0, sizeof(file) * 256);
  memcpy(&filearr[stdout->fd], open_file("/dev/tty", "w"), sizeof(node));
  memcpy(&filearr[stdin->fd], open_file("/dev/tty", "r"), sizeof(node));
  memcpy(&filearr[stderr->fd], open_file("/dev/tty", "w"), sizeof(node));
  new->open_files = filearr;
  if (last_task == NULL) {
    last_task = first_task = current_task = new;
    new->parent = 0;
  } else {
    new->parent = current_task->pid;
  }
  new->next_task = first_task;
  new->prev_task = last_task;
  last_task->next_task = new;
  last_task = new;
  first_task->prev_task = new;
  new->ring = ring;
  new->sleeping = 0;
  new->state.eflags = F_INT | F_RES;
  new->state.eip = (uint32_t)eip;
  new->pid = last_pid++;
  switch (ring) {
  case 0:
    new->state.cs = 0x08;
    new->state.ds = 0x10;
    new->state.ss = 0x10;
  case 3:
    new->state.cs = (0x18) | ring;
    new->state.ds = (0x20) | ring;
    new->state.ss = (0x20) | ring;
  }
  return new;
}

task *find_task(int pid) {
  for (task *sel = first_task; sel->pid <= pid; sel = sel->next_task) {
    if (sel->pid == pid) {
      return sel;
    }
  }
  return NULL;
}

void exit_task(task *exit, regs *arg) {
  exit->next_task->prev_task = exit->prev_task;
  exit->prev_task->next_task = exit->next_task;
  if (current_task == exit) {
    scheduler(arg);
  }
}

void go_to(task *t) { current_task = t; }

/**
 * Save the state of old task and load in new one.
 * @param arg The registers of the old task at the time of the switch.
 */
void switch_task(struct regs *arg) {
  task *candidate = current_task->next_task, *selected = NULL;
  bool preempted = false;
  if (arg->int_no == 0x20) {
    preempted =
        true; // If this function is run from the PIT, timers should be updated.
  }
  memcpy(&current_task->state, arg, sizeof(struct regs));
start:
  if (!candidate->sleeping && !candidate->stopped && !selected) {
    selected = candidate;
  }
  if (preempted && candidate->sleeping) {
    candidate->sleeping--;
  }
  if (candidate == current_task) {
    goto end;
  }
  candidate = candidate->next_task;
  goto start;
end:
  go_to(selected);
  if (current_task->state.useresp == ZERO) {
    current_task->state.useresp =
        (uint32_t)0x60000 + (size_t)(current_task->pid * 0x500);
  }
#ifdef DEBUG
  status(itoh(current_task->state.eip));
  status(itoh(current_task->state.ss));
  status(itoh(current_task->state.ds));
  status(itoh(current_task->state.useresp));
  status(itoh(current_task->state.eflags));
#endif
  memcpy(arg, &current_task->state, sizeof(struct regs));
  return;
}

file *get_file_struct(fd_t fd, task *user) {
  return user->open_files[fd].type ? (void *)(errno = -EISDIR)
                                   : user->open_files[fd].f;
}

#define SNUM arg->eax
/**
 * Run the syscall selected by the task running.
 * @param arg The registers of the task calling a syscall.
 */
uint32_t syscall_select(struct regs *arg) {
  uint32_t ret = 0;
  if (SNUM >= SYSNUM) {
    return -EINVAL;
  }
  if (SNUM == SYS_READ) { // ebx - buffer | ecx - length | edx - file
    if (arg->ebx != ZERO && arg->ecx != ZERO && arg->edx != ZERO) {
      file *accessed_file = get_file_struct((fd_t)arg->edx, current_task);
      io_op io_op_function = accessed_file->read;
      ret = (uint32_t)io_op_function((char *)arg->ebx, arg->ecx);
    } else {
      return -EINVAL;
    }
  }
  if (SNUM == SYS_WRITE) { // ebx - buffer | ecx - length | edx - file
    if (arg->ebx != ZERO && arg->ecx != ZERO && arg->edx != ZERO) {
      file *accessed_file = get_file_struct((fd_t)arg->edx, current_task);
      io_op io_op_function = accessed_file->write;
      ret = (uint32_t)io_op_function((char *)arg->ebx, arg->ecx);
    } else {
      return -EINVAL;
    }
  }
  if (SNUM == SYS_SLEEP) { // ebx - duration
    current_task->sleeping = arg->ebx;
    scheduler(arg);
    return ret;
  }
  if (SNUM == SYS_NOTE) { // ebx - frequency | ecx - duration
    add_note((note *)arg->ebx);
    current_task->sleeping = ((note *)arg->ebx)->duration;
    scheduler(arg);
    return ret;
  }
  if (SNUM == SYS_SPAWN) { // ebx - image
    new_task((int (*)(void))arg->ebx, current_task->ring);
  }
  if (SNUM == SYS_EXIT) { // ebx - status
    exit_task(current_task, arg);
    return 0;
  }
  if (SNUM == SYS_SIGNAL) {
    if (signal(arg->ebx, find_task(arg->ecx), arg)) {
      return 0;
    }
  }
  if (SNUM == SYS_GETPID) {
    ret = current_task->pid;
  }
  arg->eax = ret;
  return ret;
}
typedef void (*driver)(void);
#ifdef MODULES
// clang-format off
driver drivers[] = {
  #ifdef SOUND
  sound_update,
  #endif
  NULL
};
// clang-format on
// TODO : something about clang, this is kinda annoying.
#endif
uint32_t scheduler(struct regs *arg) {
  if (arg->int_no == 0x20) {
    for (int d = 0; drivers[d] != NULL; d++) {
      drivers[d]();
    }
    update_time();
  }
  switch_task(arg);
  return 0;
}
