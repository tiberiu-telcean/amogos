#pragma once
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <syscall.h>
#include "vfs.h"

struct __attribute__((__packed__)) tss_entry {
  uint16_t prev_tss, _prev_tss;
  uint32_t esp0;
  uint16_t ss0, _ss0;
  uint32_t esp1;      // esp and ss 1 and 2 would
  uint16_t ss1, _ss1; // be used when switching to rings 1 or 2.
  uint32_t esp2;
  uint16_t ss2, _ss2;
  uint32_t cr3;
  uint32_t eip;
  uint32_t eflags;
  uint32_t eax;
  uint32_t ecx;
  uint32_t edx;
  uint32_t ebx;
  uint32_t esp;
  uint32_t ebp;
  uint32_t esi;
  uint32_t edi;
  uint16_t es, _es;
  uint16_t cs, _cs;
  uint16_t ss, _ss;
  uint16_t ds, _ds;
  uint16_t fs, _fs;
  uint16_t gs, _gs;
  uint16_t ldt, _ldt;
  uint16_t trace, iomap_base;
};
typedef struct tss_entry tss_entry;

typedef struct gdt_entry {
  uint16_t limit_low;
  uint16_t base_low;
  uint8_t base_middle;
  uint8_t access;
  uint8_t granularity : 4;
  uint8_t limit_high : 4;
  uint8_t base_high;
} gdt_entry;

typedef struct regs {
  uint32_t ds;
  uint32_t int_no, err_code; /**< Specific to every interrupt handler */
  uint32_t edi, esi, ebp, esp, ebx, edx, ecx,
      eax;                               /**< General-purpose registers. */
  uint32_t eip, cs, eflags, useresp, ss; /**< Pused by the CPU on interrupt. */
} regs;

typedef struct task {
  struct task *next_task; /**< Linked list of tasks */
  struct task *prev_task;
  struct regs state;
  node *open_files;
  int32_t cr3;       /**< Paging structure. */
  uint16_t sleeping; /**< Number of time-slices that the process is not
                        scheduled for. */
  uint16_t pid;      /**< Process ID */
  uint16_t parent;   /**< PID of parent */
  uint8_t ring : 2;  /**< Privilege level of process */
  bool stopped;
} task;

extern void set_tss(gdt_entry *);
extern void go_to(task *);
extern file *get_file_struct(fd_t, task *);
extern task *new_task(int(eip)(void), uint8_t ring);
extern void exit_task(task *exit, regs *arg);
extern task *find_task(int pid);
extern void switch_task(struct regs *);
extern uint32_t syscall_select(struct regs *);
extern uint32_t scheduler(struct regs *);
extern void usermode(int(entry_point)(void));

/**
 * function that does nothing for the scheduler to use when everyone is
 * sleeping.
 */
extern int idler(void);
