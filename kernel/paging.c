/**
 * @file paging.c
 * @author Tiberiu Telcean <tiberiu.telcean@gmail.com>
 * @version 0.01
 * @section description
 * Set up paging in CPU.
 * @section license
 * Copyright (c) 2021 Tiberiu Telcean
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#include "paging.h"
#include "libk/stdlib.h"

page_dir_entry kernel_dir[1024];
page_table kernel_pages[1024];

void set_paging_directory(page_directory region, page_table *pages) {
  for (int i = 0; i < 1024; i++) {
    region->pagesize=0;
    region->accessed=0;
    region->address=(uint32_t)pages[i];
    region->available=0;
    region->zero=0;
    region->rw=1;
    region->user=1;
    region->cached=0;
    region->writethrough=0;
    region->present=1;
  }
}

page_table_entry prealloc[1024];

void set_table(page_table *tables) {
    for(int i=0;i<1024;i++){
        tables[i]=&prealloc[i];
        tables[i]->global=1;
        tables[i]->accessed=0;
        tables[i]->address=0x1000+i*4096;
        tables[i]->accessed=0;
        tables[i]->accessed=0;
    }
}

void set_paging(void) {
  set_table(kernel_pages);
  set_paging_directory(kernel_dir, kernel_pages);
  flush_tlb(kernel_dir);
  return;
}