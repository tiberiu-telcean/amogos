#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int sieve_main(void) {
  int number = 10000;
  int *sieve = (int *)malloc(sizeof(int) * number);
  for (int i = 2; i < number; i++) {
    if (sieve[i-2] != -1)
      sieve[i-2] = i;
    for (int j = 2 * i; j < number; j += i) {
      sieve[j-2] = -1;
    }
  }
  for (int i = 0; i < number; i++) {
    if (sieve[i] != -1) {
      printf("%d ", sieve[i]);
    }
  }
}
