#include "keyboard.h"
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <syscall.h>
#include <unistd.h>
#include <syscall.h>
#define END 1 << 30

typedef struct coords {
  int x;
  int y;
} coords;

char *map[] = {"**********  *****",
               "*......=.*  *.=.*",
               "*........*  *...*",
               "*..amogus*  *...*",
               "*........****...*",
               "*-****...|..|...*",
               "*....*...********",
               "*....*...*",
               "*....*...*",
               "**********",
               NULL};

coords vents[] = {{7, 1}, {14, 1}, {0, 0}};
char **m;
int vent = 0;
int amogus_x = 1;
int amogus_y = 1;
char stored = '.';
bool venting = false;
bool muted = true;

int *problem(int **);
char **copy(char **);
void input(int **);

void compute(void) {
  int a;
  int *inputs[5] = {&a, NULL};
  int *output;
  char *buf = (char *)malloc(sizeof(char) *
                             (strlen("Output[") + 10 + strlen("]: ") + 1));
  input(inputs);
  for (int i = 0; inputs[i] != NULL; i++) {
    printf("Input[%d]: %d", i, *inputs[i]);
  }
  output = problem(inputs);
  for (int i = 0; output[i] != END; i++) {
    printf("Output[%d]: %d", i, output[i]);
  }
}

int *problem(int **inputs) {
  int *output = (int *)malloc(sizeof(int) * 4);
  int a = *inputs[0];
  int b = *inputs[1];
  int n;
  if (((a / 10) % 10) > ((b / 10) % 10)) {
    n = a;
  } else {
    n = b;
  }
  output[0] = n;
  output[1] = END;
  return output;
}

char **copy(char **_m) {
  int i;
  char **result;
  for (i = 0; _m[i] != NULL; i++)
    ;
  result = (char **)malloc(sizeof(char **) * (i + 1));
  result[i] = NULL;
  for (i = 0; _m[i] != NULL; i++) {
    result[i] = (char *)malloc(sizeof(char **) * strlen(_m[i]));
    memcpy(result[i], _m[i], strlen(_m[i]) + 1);
  }
  return result;
}

void input(int *numbers[]) {
  int n = 0;
  keycode_t scancode, prefix;
  // scancode = key_buffer[1].scancode;
  // prefix = key_buffer[1].prefix;
  for (int i = 0; numbers[i] != NULL; i++) {
    for (int j = 0;; j++) {
      while (scancode == 0 || scancode >= 0x80)
        ;
      if (scancode == 0x1C || scancode == 0x39) {
        break;
      }
      if (scancode == 0x0E) {
        n /= 10;
        sleep(4);
        continue;
      }
      n *= 10;
      n += table[scancode] - '0';
      if (scancode >= 0x80) {
        sleep(1);
      } else {
        sleep(4);
      }
    }
    *numbers[i] = n;
    n = 0;
    sleep(5);
  }
}

struct key_bitmask cur;
void char_event(char action) {
  if (cur.Q) {
    exit(0);
  }
  if (cur.M) {
    muted = muted ? false : true;
  }
  if (cur.space) {
    for (int i = 0; vents[i].x > 0 && vents[i].y > 0; i++) {
      if (vents[i].x != amogus_x || vents[i].y != amogus_y) {
        m[vents[i].y][vents[i].x] = '1' + i;
      }
    }
    venting ^= true;
  }
}

void key_event(keycode_t action) {
  if (cur.up) {
    if (venting || m[amogus_y - 1][amogus_x] != '*')
      amogus_y = amogus_y - 1;
  }
  if (cur.left) {
    if (venting || m[amogus_y][amogus_x - 1] != '*')
      amogus_x--;
  }
  if (cur.right) {
    if (venting || m[amogus_y][amogus_x + 1] != '*')
      amogus_x++;
  }
  if (cur.down) {
    if (venting || m[amogus_y + 1][amogus_x] != '*')
      amogus_y++;
  }
}

// clang-format off
#define DEF_CHAR(_ch, _member)\
  case _ch:                   \
    if (cur._member != val) { \
      cur._member = val;      \
      char_event(_ch);        \
    }                         \
    break;
#define DEF_KEY(_x, _name)    \
  case _x:                    \
    if (cur._name != val) {   \
      cur._name = val;        \
      key_event(_x);          \
    }                         \
    break;
// clang-format on

void in_place_matrix(char **mmessage) {
  int i;
  for (i = 0; mmessage[i] != NULL; i++) {
    puts(mmessage[i]);
  }
  printf("\x1B[%dG\x1B[%dF", 1, i);
}

int music(void);

int main(void) {
  keycode_t scancode, prefix;
  bool val;
  char stored = '.';
  char buf[10];
  system(music);
  m = copy(map);
  printf("\x1B[2J\x1B[1;1H");
  for (;;) {
    // FIXME : This can be interrupted while read
    // scancode = key_buffer[0].scancode;
    // prefix = key_buffer[0].prefix;
    if (scancode > 0x80) { // PS/2 scancode set 1 uses the first bit for release
      val = false;
      scancode -= 0x80;
    } else {
      val = true;
    }
    switch (table[scancode]) {
      DEF_CHAR('A', A)
      DEF_CHAR('B', B)
      DEF_CHAR('C', C)
      DEF_CHAR('D', D)
      DEF_CHAR('E', E)
      DEF_CHAR('F', F)
      DEF_CHAR('G', G)
      DEF_CHAR('H', H)
      DEF_CHAR('I', I)
      DEF_CHAR('J', J)
      DEF_CHAR('K', K)
      DEF_CHAR('L', L)
      DEF_CHAR('M', M)
      DEF_CHAR('N', N)
      DEF_CHAR('O', O)
      DEF_CHAR('P', P)
      DEF_CHAR('Q', Q)
      DEF_CHAR('R', R)
      DEF_CHAR('S', S)
      DEF_CHAR('T', T)
      DEF_CHAR('U', U)
      DEF_CHAR('V', V)
      DEF_CHAR('W', W)
      DEF_CHAR('X', X)
      DEF_CHAR('Y', Y)
      DEF_CHAR('Z', Z)
      DEF_CHAR(' ', space)
    }
    m[amogus_y][amogus_x] = stored;
    if (prefix == 0xE0) {
      switch (scancode) {
        DEF_KEY(0x48, up)
        DEF_KEY(0x4B, left)
        DEF_KEY(0x4D, right)
        DEF_KEY(0x50, down)
      default:
        break;
      }
    }
    sleep(5);
    while (venting == true) {
      // scancode = key_buffer[0].scancode;
      if (scancode % 0x80 == 0x39) { // FIXME : could be in event handler
        break;
      }
      if ('0' <= table[scancode] && table[scancode] <= '9') {
        amogus_x = vents[table[scancode] - '1'].x;
        amogus_y = vents[table[scancode] - '1'].y;
      }
      venting = false;
      for (int i = 0; vents[i].x > 0 && vents[i].y > 0; i++) {
        m[vents[i].y][vents[i].x] = '=';
        if (vents[i].x != amogus_x || vents[i].y != amogus_y) {
          stored = 'A';
        }
      }
    }
    stored = m[amogus_y][amogus_x];
    m[amogus_y][amogus_x] = 'A';
    in_place_matrix(m);
  }
}

int btm(float beats, float bpm) { return (int)(beats * (60.0 / bpm) * 1000); }

typedef struct note {
  int duration;
  int frequency;
  bool present : 1;
  bool played : 1;
} note;
void play_note(int freq, float beats) {
  while (muted)
    ;
  note new_note =
      (note){.frequency = 0, .duration = 0, .present = true, .played = false};
  new_note.frequency = freq;
  new_note.duration = btm(beats, 100);
  csc(SYS_NOTE, (uint32_t)&new_note, NUSED, NUSED);
}

int music(void) {
  for (;;) {
    play_note(880, .5);
    play_note(1046, .5);
    play_note(1174, .5);
    play_note(1244, .5);
    play_note(1174, .5);
    play_note(1046, .5);
    play_note(880, 1);
    play_note(740, .25);
    play_note(987, .25);
    play_note(880, .5);
    sleep(btm(1, 100));
    play_note(330, .5);
    play_note(440, 1);
    play_note(880, .5);
    play_note(1046, .5);
    play_note(1174, .5);
    play_note(1244, .5);
    play_note(1174, .5);
    play_note(1046, .5);
    play_note(1174, 1);
    sleep(btm(.5, 100));

    play_note(1244, 1.0 / 3);
    play_note(1174, 1.0 / 3); // 3
    play_note(1046, 1.0 / 3);
    play_note(1244, 1.0 / 3);
    play_note(1174, 1.0 / 3); // 3
    play_note(1046, 1.0 / 3);
  }
  for (;;)
    ;
}
